﻿using System.Linq;
using NUnit.Framework;

namespace Coscine.Configuration.Tests
{
    [TestFixture]
    public class ConsulConfigurationTests
    {
        private readonly IConfiguration _configuration;

        public ConsulConfigurationTests()
        {
            // default localhost
            _configuration = new ConsulConfiguration();
        }

        [Test]
        public void ConsulConfigurationAsyncTest()
        {
            DefaultTester.ConfigurationAsyncTest(_configuration);
        }

        [Test]
        public void ConsulConfigurationAndWaitTest()
        {
            DefaultTester.ConfigurationAndWaitTest(_configuration);
        }

        [Test]
        public void ConsulConfigurationTest()
        {
            DefaultTester.ConfigurationTest(_configuration);
        }

    }
}
