﻿using Coscine.Configuration;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coscine.Configuration.Tests
{
    public class DefaultTester
    {
        public static void ConfigurationAsyncTest(IConfiguration _configuration)
        {
            string key = "TestKey";
            string value = "TheTestValue";

            Assert.IsTrue(_configuration.PutAsync(key, value).GetAwaiter().GetResult());

            var keys = _configuration.KeysAsync("").GetAwaiter().GetResult();
            Assert.IsTrue(keys.Contains(key));

            Assert.IsTrue(_configuration.GetStringAsync(key).GetAwaiter().GetResult().Equals(value));

            Assert.IsTrue(_configuration.GetStringAsync("TestingKeyNotExisting").GetAwaiter().GetResult() == null);

            Assert.IsTrue(_configuration.GetStringAsync("TestingKeyNotExisting", "default").GetAwaiter().GetResult() == "default");

            Assert.IsTrue(_configuration.DeleteAsync(key).GetAwaiter().GetResult());

            keys = _configuration.KeysAsync("").GetAwaiter().GetResult();
            Assert.IsFalse(keys.Contains(key));
        }

        public static void ConfigurationAndWaitTest(IConfiguration _configuration)
        {
            string key = "TestKey";
            string value = "TheTestValue";

            Assert.IsTrue(_configuration.PutAndWait(key, value));

            var keys = _configuration.KeysAndWait("");
            Assert.IsTrue(keys.Contains(key));

            Assert.IsTrue(_configuration.GetStringAndWait(key).Equals(value));

            Assert.IsTrue(_configuration.GetStringAndWait("TestingKeyNotExisting") == null);

            Assert.IsTrue(_configuration.GetStringAndWait("TestingKeyNotExisting", "default") == "default");

            Assert.IsTrue(_configuration.DeleteAndWait(key));

            keys = _configuration.KeysAndWait("");
            Assert.IsFalse(keys.Contains(key));
        }

        public static void ConfigurationTest(IConfiguration _configuration)
        {
            string key = "TestKey";
            string value = "TheTestValue";

            Assert.IsTrue(_configuration.Put(key, value));

            var keys = _configuration.Keys("");
            Assert.IsTrue(keys.Contains(key));

            Assert.IsTrue(_configuration.GetString(key).Equals(value));

            Assert.IsTrue(_configuration.GetString("TestingKeyNotExisting") == null);

            Assert.IsTrue(_configuration.GetString("TestingKeyNotExisting", "default") == "default");

            Assert.IsTrue(_configuration.Delete(key));

            keys = _configuration.Keys("");
            Assert.IsFalse(keys.Contains(key));
        }
    }
}
