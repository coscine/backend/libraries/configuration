﻿using Coscine.Configuration;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coscine.Configuration.Tests
{
    public class EnvironmentConfigurationTests
    {
        private readonly IConfiguration _configuration;

        public EnvironmentConfigurationTests()
        {
            _configuration = new EnvironmentConfiguration(EnvironmentVariableTarget.Process);
        }

        [Test]
        public void EnvironmentConfigurationAsyncTest()
        {
            DefaultTester.ConfigurationAsyncTest(_configuration);
        }

        [Test]
        public void EnvironmentConfigurationAndWaitTest()
        {
            DefaultTester.ConfigurationAndWaitTest(_configuration);
        }

        [Test]
        public void EnvironmentConfigurationTest()
        {
            DefaultTester.ConfigurationTest(_configuration);
        }
    }
}
