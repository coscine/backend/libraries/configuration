﻿using System.Threading.Tasks;

namespace Coscine.Configuration
{
    public interface IConfiguration
    {
        Task<bool> PutAsync(string key, string value);

        Task<bool> PutAsync(string key, byte[] value);

        bool PutAndWait(string key, string value);

        bool PutAndWait(string key, byte[] value);

        Task<byte[]> GetAsync(string key);

        byte[] GetAndWait(string key);

        Task<string> GetStringAsync(string key);

        Task<string> GetStringAsync(string key, string defaultValue);

        string GetStringAndWait(string key);

        string GetStringAndWait(string key, string defaultValue);

        Task<string[]> KeysAsync(string prefix);

        string[] KeysAndWait(string prefix);

        Task<bool> DeleteAsync(string key);

        bool DeleteAndWait(string key);

        bool Put(string key, string value);

        bool Put(string key, byte[] value);

        byte[] Get(string key);

        string GetString(string key);

        string GetString(string key, string defaultValue);

        string[] Keys(string prefix);

        bool Delete(string key);
    }
}
