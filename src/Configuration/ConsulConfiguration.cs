﻿using Consul;
using System;
using System.Text;
using System.Threading.Tasks;

namespace Coscine.Configuration
{
    public class ConsulConfiguration : IConfiguration
    {
        public Action<ConsulClientConfiguration> Configuration { get; set; }

        public ConsulConfiguration() : this(null)
        {

        }

        public ConsulConfiguration(Action<ConsulClientConfiguration> configuration)
        {
            Configuration = configuration;
        }

        public async Task<bool> PutAsync(string key, string value)
        {
            return await PutAsync(key, Encoding.UTF8.GetBytes(value));
        }

        public async Task<bool> PutAsync(string key, byte[] value)
        {
            var putPair = new KVPair(key)
            {
                Value = value
            };

            using (var client = new ConsulClient(Configuration))
            {
                var putAttempt = await client.KV.Put(putPair);

                return putAttempt.Response;
            }
        }


        public bool PutAndWait(string key, byte[] value)
        {
            var putPair = new KVPair(key)
            {
                Value = value
            };

            using (var client = new ConsulClient(Configuration))
            {
                var putAttempt = client.KV.Put(putPair);

                putAttempt.Wait();

                return putAttempt.Result.Response;
            }
        }

        public bool PutAndWait(string key, string value)
        {
            return PutAndWait(key, Encoding.UTF8.GetBytes(value));
        }

        public async Task<byte[]> GetAsync(string key)
        {
            using (var client = new ConsulClient(Configuration))
            {
                var getPair = await client.KV.Get(key);

                if (getPair.Response?.Value != null)
                {
                    return getPair.Response.Value;
                }
                else
                {
                    // TODO: Add logging that key has not been found
                    return null;
                }
            }
        }

        public byte[] GetAndWait(string key)
        {
            using (var client = new ConsulClient(Configuration))
            {
                var getPair = client.KV.Get(key);

                getPair.Wait();

                if (getPair.Result.Response?.Value != null)
                {
                    return getPair.Result.Response.Value;
                }
                else
                {
                    // TODO: Add logging that key has not been found
                    return null;
                }
            }
        }

        public async Task<string> GetStringAsync(string key)
        {
            var value = await GetAsync(key);
            if(value != null)
            {
                return Encoding.UTF8.GetString(value, 0, value.Length);
            }
            else
            {
                return null;
            }
        }

        public async Task<string> GetStringAsync(string key, string defaultValue)
        {
            var value = await GetStringAsync(key);
            if (value == null)
            {
                // TODO: Log use of default value
                return defaultValue;
            }
            return value;
        }

        public string GetStringAndWait(string key)
        {
            var value = GetAndWait(key);
            if (value != null)
            {
                return Encoding.UTF8.GetString(value, 0, value.Length);
            }
            else
            {
                return null;
            }
        }

        public string GetStringAndWait(string key, string defaultValue)
        {
            var value = GetStringAndWait(key);
            if (value == null)
            {
                // TODO: Log use of default value
                return defaultValue;
            }
            return value;
        }

        public async Task<string[]> KeysAsync(string prefix)
        {
            using (var client = new ConsulClient(Configuration))
            {
                var keys = await client.KV.Keys(prefix);
                return keys.Response;
            }
        }

        public string[] KeysAndWait(string prefix)
        {
            using (var client = new ConsulClient(Configuration))
            {
                var keys = client.KV.Keys(prefix);
                keys.Wait();
                return keys.Result.Response;
            }
        }

        public async Task<bool> DeleteAsync(string key)
        {
            using (var client = new ConsulClient(Configuration))
            {
                var deleteRequest = await client.KV.Delete(key);
                return deleteRequest.Response;
            }
        }

        public bool DeleteAndWait(string key)
        {
            using (var client = new ConsulClient(Configuration))
            {
                var deleteRequest = client.KV.Delete(key);
                deleteRequest.Wait();
                return deleteRequest.Result.Response;
            }
        }

        public bool Put(string key, string value)
        {
            return PutAsync(key, value).GetAwaiter().GetResult();
        }

        public bool Put(string key, byte[] value)
        {
            return PutAsync(key, value).GetAwaiter().GetResult();
        }

        public byte[] Get(string key)
        {
            return GetAsync(key).GetAwaiter().GetResult();
        }

        public string GetString(string key)
        {
            return GetStringAsync(key).GetAwaiter().GetResult();
        }

        public string GetString(string key, string defaultValue)
        {
            return GetStringAsync(key, defaultValue).GetAwaiter().GetResult();
        }

        public string[] Keys(string prefix)
        {
            return KeysAsync(prefix).GetAwaiter().GetResult();
        }

        public bool Delete(string key)
        {
            return DeleteAsync(key).GetAwaiter().GetResult();
        }
    }
}
