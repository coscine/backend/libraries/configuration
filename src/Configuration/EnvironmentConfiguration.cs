﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Coscine.Configuration
{
    public class EnvironmentConfiguration : IConfiguration
    {
        private readonly EnvironmentVariableTarget target;

        public EnvironmentConfiguration()
        {
            target = EnvironmentVariableTarget.Machine;
        }

        public EnvironmentConfiguration(EnvironmentVariableTarget target)
        {
            this.target = target;
        }

        public bool Delete(string key)
        {
            try
            {
                Environment.SetEnvironmentVariable(key, null, target);
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public bool DeleteAndWait(string key)
        {
            return Delete(key);
        }

        public Task<bool> DeleteAsync(string key)
        {
            return Task.Factory.StartNew(() =>
            {
                return Delete(key);
            });
        }

        public byte[] Get(string key)
        {
            var result = GetString(key);
            if (result != null)
            {
                return new System.Text.ASCIIEncoding().GetBytes(result);
            }
            else
            {
                return null;
            }
        }

        public byte[] GetAndWait(string key)
        {
            return Get(key);
        }

        public Task<byte[]> GetAsync(string key)
        {
            return Task.Factory.StartNew(() =>
            {
                return Get(key);
            });
        }

        public string GetString(string key)
        {
            try
            {
                return Environment.GetEnvironmentVariable(key, target);
            }
            catch (Exception)
            {
                // TODO: Add logging that key has not been found
                return null;
            }
        }

        public string GetString(string key, string defaultValue)
        {
            var value = GetString(key);
            if(value == null)
            {
                // TODO: Log Default value usage
                return defaultValue;
            }
            return value;
        }

        public string GetStringAndWait(string key)
        {
            return GetString(key);
        }

        public string GetStringAndWait(string key, string defaultValue)
        {
            return GetString(key, defaultValue);
        }

        public async Task<string> GetStringAsync(string key)
        {
            return await Task.Factory.StartNew(() =>
            {
                return GetString(key);
            });
        }

        public async Task<string> GetStringAsync(string key, string defaultValue)
        {
            return await Task.Factory.StartNew(() =>
            {
                return GetString(key, defaultValue);
            });
        }

        public string[] Keys(string prefix)
        {
            IDictionary envVars = Environment.GetEnvironmentVariables();
            List<string> filteredKeys = new List<string>();
            foreach (DictionaryEntry envVar in envVars)
            {
                if (envVar.Key.ToString().StartsWith(prefix))
                {
                    filteredKeys.Add(envVar.Key.ToString());
                }
            }
            return filteredKeys.ToArray();
        }

        public string[] KeysAndWait(string prefix)
        {
            return Keys(prefix);
        }

        public Task<string[]> KeysAsync(string prefix)
        {
            return Task.Factory.StartNew(() =>
            {
                return Keys(prefix);
            });
        }

        public bool Put(string key, string value)
        {
            try
            {
                Environment.SetEnvironmentVariable(key, value, target);
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public bool Put(string key, byte[] value)
        {
            return Put(key, new System.Text.ASCIIEncoding().GetString(value));
        }

        public bool PutAndWait(string key, byte[] value)
        {
            return Put(key, value);
        }

        public bool PutAndWait(string key, string value)
        {
            return Put(key, value);
        }

        public Task<bool> PutAsync(string key, string value)
        {
            return Task.Factory.StartNew(() =>
            {
                return Put(key, value);
            });
        }

        public Task<bool> PutAsync(string key, byte[] value)
        {
            return Task.Factory.StartNew(() =>
            {
                return Put(key, value);
            });
        }
    }
}
